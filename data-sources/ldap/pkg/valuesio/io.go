package valuesio

import "gitlab.cern.ch/thartlan/ldap-data-source/pkg/types"

type IO interface {
	GetValues() (*types.InputValues, error)
	WriteValues(*types.OutputValues) error
}
