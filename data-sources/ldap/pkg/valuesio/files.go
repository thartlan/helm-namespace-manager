package valuesio

import (
	"fmt"
	"io"
	"os"

	"gitlab.cern.ch/thartlan/ldap-data-source/pkg/types"

	"gopkg.in/yaml.v2"
)

type Files struct {
	InputFilename  string
	OutputFilename string
}

func (f *Files) GetValues() (*types.InputValues, error) {
	inputFile, err := os.Open(f.InputFilename)
	if err != nil {
		return nil, fmt.Errorf("failed to open file %q: %v", f.InputFilename, err)
	}
	inputData, err := io.ReadAll(inputFile)
	if err != nil {
		return nil, fmt.Errorf("failed to read data from file: %v", err)
	}

	var values types.InputValues
	err = yaml.Unmarshal(inputData, &values)
	if err != nil {
		return nil, fmt.Errorf("failed to parse values yaml: %v", err)
	}
	return &values, err
}

func (f *Files) WriteValues(values *types.OutputValues) error {
	output, err := yaml.Marshal(&values)
	if err != nil {
		return fmt.Errorf("failed to marshal output yaml: %v", err)
	}
	if f.OutputFilename == "" {
		fmt.Println(string(output))
		return nil
	}
	outputFile, err := os.Create(f.OutputFilename)
	if err != nil {
		return fmt.Errorf("failed to open output file %q: %v", f.OutputFilename, err)
	}
	fmt.Fprintf(outputFile, string(output))
	return nil
}
