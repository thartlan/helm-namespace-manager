package main

import (
	"fmt"

	"gitlab.cern.ch/thartlan/ldap-data-source/pkg/types"

	goldap "github.com/go-ldap/ldap"
)

type Conn struct {
	*goldap.Conn
}

func NewLDAPClient(url string) (*Conn, error) {
	conn, err := goldap.DialURL(url)
	if err != nil {
		return nil, err
	}
	return &Conn{conn}, nil
}

func (l *Conn) GetUsersInEgroup(name string) ([]*goldap.Entry, error) {
	searchRequest := goldap.NewSearchRequest(
		"OU=Users,OU=Organic Units,DC=cern,DC=ch",
		goldap.ScopeWholeSubtree, goldap.NeverDerefAliases,
		0, 0, false,
		fmt.Sprintf("(&(objectClass=user)(memberOf=CN=%s,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))", name),
		[]string{},
		nil,
	)

	var sr *goldap.SearchResult
	var err error
	sr, err = l.Search(searchRequest)
	if err != nil {
		return nil, err
	}

	return sr.Entries, nil
}

func (l *Conn) validateGroup(group string, maxSize int) error {
	searchRequest := goldap.NewSearchRequest(
		"OU=e-groups,OU=Workgroups,DC=cern,DC=ch",
		goldap.ScopeWholeSubtree, goldap.NeverDerefAliases,
		0, 0, false,
		fmt.Sprintf("(&(objectClass=group)(cn=%s))", group),
		[]string{},
		nil,
	)

	sr, err := l.Search(searchRequest)
	if err != nil {
		return fmt.Errorf("failed search for group: %v", err)
	}

	if len(sr.Entries) == 0 {
		return fmt.Errorf("group not found")
	}

	if len(sr.Entries) > 1 {
		return fmt.Errorf("%d groups found", len(sr.Entries))
	}

	groupEntry := sr.Entries[0]

	numMembers := len(groupEntry.GetAttributeValues("member"))
	if maxSize > 0 && numMembers > maxSize {
		return fmt.Errorf("group members exceeds the safety limit (%d>%d)", numMembers, maxSize)
	}
	return nil
}

func (l *Conn) GetDataFor(groups, users []string, properties []string, maxSize int) (types.GroupData, types.UserData, error) {
	groupMembers := make(map[string][]string)
	userData := make(map[string]map[string]string)
	for _, group := range groups {
		if err := l.validateGroup(group, maxSize); err != nil {
			return nil, nil, fmt.Errorf("group validation for %q failed: %v", group, err)
		}
		members, err := l.GetUsersInEgroup(group)
		if err != nil {
			return nil, nil, err
		}
		var memberNames []string
		for _, member := range members {
			name := member.GetAttributeValue("cn")
			memberNames = append(memberNames, name)
			if _, ok := userData[name]; ok {
				continue
			}
			user := make(map[string]string)
			for _, prop := range properties {
				user[prop] = member.GetAttributeValue(prop)
			}
			userData[name] = user
		}
		groupMembers[group] = memberNames

	}
	return groupMembers, userData, nil
}
