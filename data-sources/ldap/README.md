# ldap-data-source

Extracts group names from a values file and dumps user/group data into a second values file.

Can read out of and into Kubernetes ConfigMaps (containing a values.yaml).

### Example

```
$ cat values.yaml 
personalNamespaces:
- group: it-dep-cm-rps

$ ./ldap-data-source --input-file values.yaml
data:
  groups:
    it-dep-cm-rps:
    - thartlan
    - ...
  users:
    thartlan:
      mail: thomas.george.hartland@cern.ch
    ...
```

### Usage

```
Usage of ./ldap-data-source:
      --input-configmap string    input configmap name
      --input-file string         input values file
      --kubeconfig string         kubeconfig file (leave blank for in-cluster config)
      --ldap-group-max-size int   safety limit for number of members in a group (default 200)
      --ldap-properties strings   list of ldap properties to copy into output values (default [mail])
      --ldap-url string           LDAP url to connect to (default "ldap://xldap.cern.ch:389")
      --namespace string          namespace name to read/write configmap (default "default")
      --output-configmap string   output configmap name
      --output-file string        output values file
```
