package main

import (
	"gitlab.cern.ch/thartlan/ldap-data-source/pkg/types"
	"gitlab.cern.ch/thartlan/ldap-data-source/pkg/valuesio"

	"github.com/spf13/pflag"
)

var (
	inputFilename  string
	outputFilename string

	inputConfigMapName  string
	outputConfigMapName string
	namespace           string
	kubeconfig          string

	ldapURL          string
	ldapProperties   []string
	ldapGroupMaxSize int
)

func init() {
	pflag.StringVar(&inputFilename, "input-file", "", "input values file")
	pflag.StringVar(&outputFilename, "output-file", "", "output values file")
	pflag.StringVar(&inputConfigMapName, "input-configmap", "", "input configmap name")
	pflag.StringVar(&outputConfigMapName, "output-configmap", "", "output configmap name")
	pflag.StringVar(&namespace, "namespace", "default", "namespace name to read/write configmap")
	pflag.StringVar(&kubeconfig, "kubeconfig", "", "kubeconfig file (leave blank for in-cluster config)")
	pflag.StringVar(&ldapURL, "ldap-url", "ldap://xldap.cern.ch:389", "LDAP url to connect to")
	pflag.StringSliceVar(&ldapProperties, "ldap-properties", []string{"mail"}, "list of ldap properties to copy into output values")
	pflag.IntVar(&ldapGroupMaxSize, "ldap-group-max-size", 200, "safety limit for number of members in a group")
}

func main() {
	pflag.Parse()

	var valuesIO valuesio.IO
	var err error

	if inputFilename != "" {
		valuesIO = &valuesio.Files{inputFilename, outputFilename}
	} else if inputConfigMapName != "" && outputConfigMapName != "" {
		valuesIO, err = valuesio.NewConfigMaps(inputConfigMapName, outputConfigMapName, kubeconfig, namespace)
		if err != nil {
			panic(err)
		}
	} else {
		panic("no valid input/output specified")
	}

	values, err := valuesIO.GetValues()
	if err != nil {
		panic(err)
	}

	var groups, users []string
	for _, group := range values.PersonalNamespaces {
		groups = append(groups, group.Group)
	}

	var outputValues types.OutputValues

	ldapConn, err := NewLDAPClient(ldapURL)
	if err != nil {
		panic(err)
	}

	groupData, userData, err := ldapConn.GetDataFor(groups, users, ldapProperties, ldapGroupMaxSize)
	if err != nil {
		panic(err)
	}

	outputValues.Data.Groups = groupData
	outputValues.Data.Users = userData

	err = valuesIO.WriteValues(&outputValues)
	if err != nil {
		panic(err)
	}
}
